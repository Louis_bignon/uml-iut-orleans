public class Carrote implements INourriture {
    
    private int energieDonne;
    private int nbCarrotte;


    public Carrote(){
        this.nbCarrotte = 0;
        this.energieDonne = 1;
    }

    public void pousser(){
        this.nbCarrotte += 1;
    }

    public int getNbNourriture(){
        return this.nbCarrotte;
    }

    public int getEnergieDonne(){
        return this.energieDonne;
    }

    public void baisserNbNourriture(int nbNourriture){
        this.nbCarrotte -=nbNourriture;
    }


}

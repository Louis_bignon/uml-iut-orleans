import java.util.Random;


public class Animal implements IEvenement {
    

    private String sexe;
    private int age ;
    private AutomateAnimal automate;
    private int plafondEnergie;
    private int soldeEnergie;
    private Prairie prairieCourante;
    private ISeDeplacer methodeDeplacement;
    private ISeNourir methodeNourir; 
    private boolean mort;


    public Animal(AutomateAnimal a){
        String[] arr={"Mâle","Femelle"}; 
        Random r=new Random(); 
        int randomNumber=r.nextInt(arr.length);

        this.age = 0;
        this.automate = a;
        this.plafondEnergie = 40;
        this.soldeEnergie = 40;
        this.sexe = arr[randomNumber];
        this.methodeDeplacement = new DeplacementHerbivore();  
        this.methodeNourir = new Herbivore(); 
        this.mort = false;
    }


    //EVENEMENTS
    public void deplacement(ISeDeplacer methodeDeplacer){
        this.automate.setAnimalCourant(this);
        this.automate.deplacement(methodeDeplacer);  
    } 
    public void nourir(ISeNourir methodeSeNourir){
        this.automate.setAnimalCourant(this);
        this.automate.nourir(methodeSeNourir);
    }
    public void reproduire(){
        this.automate.reproduire();
    }


    //ACTIONS
    public void seDeplacer(ISeDeplacer methodeDeplacer){
        methodeDeplacer.seDeplacer(this);
    }

    public void seNourir(ISeNourir methodeNourir){
        methodeNourir.seNourir(this);
    }

    public void seReproduire(Animal animalB){

        this.baisserEnergie(3);
        animalB.baisserEnergie(3);
        Animal bebe = new Animal(this.automate);
        this.prairieCourante.addAnimal(bebe);
    }

    // GETTEUR 
    public int getSolde(){
        return this.soldeEnergie;
    }

    public int getPlafond(){
        return this.plafondEnergie;
    }

    public String getSexe(){
        return this.sexe;
    }

    public int getAge(){
        return this.age;
    }

    public String getInfosAnimal(){
        return "Animal :"+ this.getSexe() +" | age :"+this.getAge()+" | energies :"+this.getSolde();
    }

    public AutomateAnimal getAutomate(){
        return this.automate;
    }

    

    public ISeDeplacer getMethodeDeplacement(){
        return this.methodeDeplacement;
    }

    public ISeNourir getMethodeNourriture(){
        return this.methodeNourir;
    }


    public Prairie getPrairieCourante(){
        return this.prairieCourante;
    }

    public boolean getMort(){
        return this.mort;
    }

    //SETTER
    public void setSolde(int valeur){
        this.soldeEnergie = valeur;

    }

    public void setPrairie(Prairie prairie){
        this.prairieCourante = prairie;
    }

    public void augmenterAge(){
        this.age += 1;
    }
    public void setMort(){
        this.mort = true;
    }

    //METHODS

    public void baisserEnergie(int valeur){
        this.soldeEnergie -= valeur;
        if (this.soldeEnergie <= 0){
            this.soldeEnergie = 0;
        }
    }

    public void augmenterEnergie(int valeur){
        this.soldeEnergie += valeur;
        if (this.soldeEnergie >= this.plafondEnergie){
            this.soldeEnergie = this.plafondEnergie;
        }
    }

    public void passerJourne(){
        this.baisserEnergie(1);
        this.deplacement(this.methodeDeplacement);
        this.nourir(this.methodeNourir);
        this.reproduire();
        
    }


}

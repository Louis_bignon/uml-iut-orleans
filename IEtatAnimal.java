public interface IEtatAnimal {

    public void deplacement(AutomateAnimal automateA, ISeDeplacer methodeDeplacer);

    public void nourir(AutomateAnimal automateB,ISeNourir methodeSeNourir);

    public void reproduire(AutomateAnimal automateC);
}

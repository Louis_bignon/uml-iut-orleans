import java.util.List;

import java.util.ArrayList;

public class Prairie {

    private int i;
    private int j;
    private List<Animal> listeAnimaux;
    private Monde monde;
    private List<INourriture> listeNourriture;

    public Prairie(int i, int j, Monde monde){
        this.listeAnimaux = new ArrayList<>();
        this.i = i ;
        this.j = j;
        this.listeNourriture = new ArrayList<>();
        this.listeNourriture.add(new Carrote());
        this.monde = monde;
    }



    @Override
    public String toString(){
        String res = "     " ;
        for (Animal animal : this.listeAnimaux){
            res += "(";
            res += animal.getInfosAnimal();
            res += ") ";
        }
        // res+= "  |";
        return res;
    }

    public void addAnimal(Animal animal){

        this.listeAnimaux.add(animal);
        for(Animal a : this.listeAnimaux){
            a.setPrairie(this);
        }
    }

    public void pousser(){
        for (INourriture nourriture : this.listeNourriture){
            nourriture.pousser();
        }
    }

    public void baisserNbNourriture(int indexNourriture, int nbNourriture){
        this.listeNourriture.get(indexNourriture).baisserNbNourriture(nbNourriture);
    }

    public void removeAnimal(Animal animal){
        this.listeAnimaux.remove(animal);
    }



    //GETTEUR
    public List<Animal> getAnimal(){
        return this.listeAnimaux;
    }

    public int getI(){
        return this.i;
    }
    public int getJ(){
        return this.j;
    }

    public Monde getMonde(){
        return this.monde;
    }
    public List<INourriture> getListeNourriture(){
        return this.listeNourriture;
    }

    public int getNbNourriture(){
        int res = 0;
        for(INourriture i : this.listeNourriture){
            res += i.getNbNourriture();
        }
        return res;
    }

    public List<Animal> getAnimalParSexe(String sexe){
        List<Animal> animalSelonSexe = new ArrayList<>();

        for (Animal a : this.listeAnimaux){
            if (a.getSexe() == sexe){
                animalSelonSexe.add(a);
            }
        }
        return animalSelonSexe;
    }

    public List<Animal> getListeParRace(String race){
        List<Animal> listeAnimalRace = new ArrayList<>();
        for (Animal a : this.listeAnimaux){
            if (a.getClass().getName() == race){
                listeAnimalRace.add(a);
            }
        }
        return listeAnimalRace;
    }

}

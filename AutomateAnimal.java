public class AutomateAnimal implements IEvenement {

    private IEtatAnimal etatcourant;
    private Animal animal;

    public AutomateAnimal(){
        this.etatcourant = new AdulteRassasie();
    }

    public void changementEtat(IEtatAnimal etat){
        this.etatcourant = etat;
    }

    public Animal getAnimal(){
        return this.animal;
    }
    public IEtatAnimal getEtatCourant(){
        return this.etatcourant;
    }

    public void setAnimalCourant(Animal animal){
        this.animal = animal;
    }

    

    //EVENEMENT
    public void deplacement(ISeDeplacer methodeDeplacer){
        this.etatcourant.deplacement(this, methodeDeplacer);
    }

    public void nourir(ISeNourir methodeSeNourir){
        this.etatcourant.nourir(this,methodeSeNourir);
    }
    public void reproduire(){
        this.etatcourant.reproduire(this);
    }
}

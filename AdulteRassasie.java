import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class AdulteRassasie implements IEtatAnimal {
    

    public void deplacement(AutomateAnimal automateA, ISeDeplacer methodeDeplacer){
        Animal animal = automateA.getAnimal();
        animal.seDeplacer(methodeDeplacer);
        if (animal.getSolde() <= 10){
            IEtatAnimal etatAffame = new AdulteAffame();
            automateA.changementEtat(etatAffame);
        }
    }

    public void nourir(AutomateAnimal automateB,ISeNourir methodeSeNourir){
        // Animal animal = automateB.getAnimal();
        
        // if (animal.getSolde() < animal.getPlafond()){
        //     animal.seNourir(methodeSeNourir);
        // }
    }

    public void reproduire(AutomateAnimal automateC){
        Animal animalA = automateC.getAnimal();
        if (animalA.getAge() >=5){
   
            Prairie pCourante = animalA.getPrairieCourante();
            List<Animal> sexeOppose = new ArrayList<>();
    
            if (animalA.getSexe() == "Mâle"){
                sexeOppose = pCourante.getAnimalParSexe("Femelle");
            }
            else{
                sexeOppose = pCourante.getAnimalParSexe("Mâle");
            }
    
            if(sexeOppose.size() != 0){
                Random r = new Random();
                Animal animalB = sexeOppose.get(r.nextInt(sexeOppose.size()));
                animalA.seReproduire(animalB);
            }
            if (animalA.getSolde() < 10){
                IEtatAnimal adulteAffame = new AdulteAffame();
                automateC.changementEtat(adulteAffame);
            }
        }

    }
}

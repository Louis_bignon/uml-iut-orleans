public interface INourriture {

    public void pousser();

    public int getNbNourriture();

    public int getEnergieDonne();

    public void baisserNbNourriture(int nbNourriture);


}

public class Renard extends Animal {
    private ISeDeplacer methodeDeplacement;
    private ISeNourir methodeNourir;
    private AutomateAnimal automate;
    
    public Renard(AutomateAnimal automate){
        super(automate);
        this.methodeDeplacement = new DeplacementCarnivore();
        this.methodeNourir = new Carnivore();
        this.automate = automate;
    }

    public void seDeplacer(){
        this.methodeDeplacement.seDeplacer(this);
    }

    public void seNourir(){
        this.methodeNourir.seNourir(this);
    }

    public void seReproduire(Animal animalB){
        this.baisserEnergie(3);
        animalB.baisserEnergie(3);
        Renard bebe = new Renard(this.automate);
        this.getPrairieCourante().addAnimal(bebe);
    }
    public void passerJourne(){
        this.baisserEnergie(1);
        this.deplacement(this.methodeDeplacement);
        this.nourir(this.methodeNourir);
        this.reproduire();
        
    }



    public String getInfosAnimal(){
        return "R :"+this.getSolde();
    }



}

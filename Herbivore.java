import java.util.List;

public class Herbivore implements ISeNourir {
    
    public void seNourir(Animal animal){

        Prairie prairieCourante = animal.getPrairieCourante();
        List<INourriture> listeNourriture = prairieCourante.getListeNourriture(); 

        if(listeNourriture.size() != 0){
            int index = 0;
            for (int i = 0 ; i < listeNourriture.size() ; i ++){
                if ((listeNourriture.get(i).getNbNourriture()*listeNourriture.get(i).getEnergieDonne()) > listeNourriture.get(index).getNbNourriture()*listeNourriture.get(index).getEnergieDonne()){
                    index = i;
                    
                }
            }
            animal.augmenterEnergie(listeNourriture.get(index).getEnergieDonne()*listeNourriture.get(index).getNbNourriture());
            prairieCourante.baisserNbNourriture(index, listeNourriture.get(index).getNbNourriture());

        }
    }
}



public class Lapin extends Animal {
    

    private ISeDeplacer methodeDeplacement;
    private ISeNourir methodeNourir;
    private AutomateAnimal automate;

    public Lapin(AutomateAnimal automate){
        super(automate);
        this.methodeDeplacement = new DeplacementHerbivore();
        this.methodeNourir = new Herbivore();
        this.automate = automate;
    }

    public void seDeplacer(){
        this.methodeDeplacement.seDeplacer(this);
    }

    public void seNourir(){
        this.methodeNourir.seNourir(this);
    }

    public void seReproduire(Animal animalB){

        this.baisserEnergie(3);
        animalB.baisserEnergie(3);
        Lapin bebe = new Lapin(this.automate);
        this.getPrairieCourante().addAnimal(bebe);;
    }

    public void passerJourne(){
        this.baisserEnergie(1);
        this.deplacement(this.methodeDeplacement);
        this.nourir(this.methodeNourir);
        this.reproduire();
        
    }


    //Getteur

    // public String getInfosAnimal(){
    //     return "Lapin :"+ this.getSexe() +" | age :"+this.getAge()+" | energies :"+this.getSolde();
    // }

    public String getInfosAnimal(){
        return "L:"+this.getSolde();
    }


}

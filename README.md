# Mini-projet Rabbit&Co

## Explication diagrammes :
![diagramme1](static/uml1.png)

J'ai choisis de controler mes animaux grâce à un pattern state ainsi qu'un pattern strategy afin de permettre une meilleur prise en charge de possibles modifications. Il sera pas la suite simple de rajouter d'autres animaux, par exemple des renards.

![diagramme1](static/uml2.png)
*Ici les renards sont implémentés*

Pour se qui est du déplacement des animaux, je suis parti du principe qu'il sera possible un jour d'implémenter des animaux qui ne se déplace pas de la même manière. Grâce au pattern strategy implémenté dans le diagramme, cette modification sera possible.

## Niveau d'avancement du projet :

Les lapins ainsi que les renards ont été implémenté avec succès. C'est à dire qu'il leurs est possible de se déplacer, se nourrir ainsi que de se reproduire. Seul le déplacement des bébés renard qui doivent suivre leurs mère n'a pas été implémenté par faute de temps.

## Comment lancer le projet :
1. Télécharger le dépot git grâce au lien dans le dépot célène.
2. Lancer la class MyAnimal.java
Après ça, des choix simples vont seront proposé pour choisir le nombre de renard et le nombre de lapin à mettre sur votre monde.

## Diagramme état-transition :
![diagramme1](static/uml3.png)

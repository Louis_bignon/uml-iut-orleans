
public interface IEvenement {
    

    public void deplacement(ISeDeplacer methodeDeplacer);
    public void nourir(ISeNourir methodeSeNourir);
    public void reproduire();
}

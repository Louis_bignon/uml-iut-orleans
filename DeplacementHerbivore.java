import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class DeplacementHerbivore implements ISeDeplacer {
    

    public void seDeplacer(Animal animal){
        animal.baisserEnergie(1);
        if (animal.getAutomate().getEtatCourant().toString()== "AdulteAffame" ){
            Prairie prairieCourante = animal.getPrairieCourante();
            Prairie prairieArrive = this.trouverNourriture(animal);
            prairieArrive.addAnimal(animal);
            prairieCourante.removeAnimal(animal); 
        }
        else{
            Prairie prairieCourante = animal.getPrairieCourante();
            Prairie prairieArrive = this.trouverReproduction(animal);
            prairieArrive.addAnimal(animal);
            prairieCourante.removeAnimal(animal); 
        }

    }
    
    public Prairie trouverReproduction(Animal animal){
        Prairie prairieCourante = animal.getPrairieCourante();
        Monde monde = prairieCourante.getMonde();
        Prairie[][] matricePrairie = monde.getPrairies();
        Random r = new Random();

        if (prairieCourante.getI() > 0 && prairieCourante.getI() < matricePrairie.length-1 && prairieCourante.getJ() > 0 && prairieCourante.getJ() < matricePrairie[0].length-1){
            Prairie iPlus1 = matricePrairie[prairieCourante.getI()+1][prairieCourante.getJ()];
            Prairie iMoin1 = matricePrairie[prairieCourante.getI()-1][prairieCourante.getJ()];
            Prairie jPlus1 = matricePrairie[prairieCourante.getI()][prairieCourante.getJ()+1];
            Prairie jMoin1 = matricePrairie[prairieCourante.getI()][prairieCourante.getJ()-1];

            List<Prairie> listePrairieSuivante = new ArrayList<>();
            listePrairieSuivante.add(iPlus1);
            listePrairieSuivante.add(iMoin1);
            listePrairieSuivante.add(jPlus1);
            listePrairieSuivante.add(jMoin1);

            Prairie pSuivante = listePrairieSuivante.get(r.nextInt(listePrairieSuivante.size())) ; 
            for (Prairie p : listePrairieSuivante){
                if (animal.getSexe() == "Mâle"){
                    if (pSuivante.getAnimalParSexe("Femelle").size() < p.getAnimalParSexe("Femelle").size()){
                        pSuivante = p;
                    }
                }
                else{
                    if (pSuivante.getAnimalParSexe("Mâle").size() < p.getAnimalParSexe("Mâle").size()){
                        pSuivante = p;
                    }
                }
            }

            return pSuivante;
        }
        else if (prairieCourante.getI() <= 0 && prairieCourante.getJ() > 0 && prairieCourante.getJ() < matricePrairie[0].length-1){
            Prairie iPlus1 = matricePrairie[prairieCourante.getI()+1][prairieCourante.getJ()];
            Prairie jPlus1 = matricePrairie[prairieCourante.getI()][prairieCourante.getJ()+1];
            Prairie jMoin1 = matricePrairie[prairieCourante.getI()][prairieCourante.getJ()-1];
            
            List<Prairie> listePrairieSuivante = new ArrayList<>();
            listePrairieSuivante.add(iPlus1);
            listePrairieSuivante.add(jPlus1);
            listePrairieSuivante.add(jMoin1);

            Prairie pSuivante = listePrairieSuivante.get(r.nextInt(listePrairieSuivante.size())) ; 
            for (Prairie p : listePrairieSuivante){
                if (animal.getSexe() == "Mâle"){
                    if (pSuivante.getAnimalParSexe("Femelle").size() < p.getAnimalParSexe("Femelle").size()){
                        pSuivante = p;
                    }
                }
                else{
                    if (pSuivante.getAnimalParSexe("Mâle").size() < p.getAnimalParSexe("Mâle").size()){
                        pSuivante = p;
                    }
                }
            }

            return pSuivante;
        }
        else if(prairieCourante.getI() >= matricePrairie.length-1 && prairieCourante.getJ() > 0 && prairieCourante.getJ() < matricePrairie[0].length-1){
            Prairie iMoin1 = matricePrairie[prairieCourante.getI()-1][prairieCourante.getJ()];
            Prairie jPlus1 = matricePrairie[prairieCourante.getI()][prairieCourante.getJ()+1];
            Prairie jMoin1 = matricePrairie[prairieCourante.getI()][prairieCourante.getJ()-1];

            List<Prairie> listePrairieSuivante = new ArrayList<>();
            listePrairieSuivante.add(iMoin1);
            listePrairieSuivante.add(jPlus1);
            listePrairieSuivante.add(jMoin1);

            Prairie pSuivante = listePrairieSuivante.get(r.nextInt(listePrairieSuivante.size())) ; 
            for (Prairie p : listePrairieSuivante){
                if (animal.getSexe() == "Mâle"){
                    if (pSuivante.getAnimalParSexe("Femelle").size() < p.getAnimalParSexe("Femelle").size()){
                        pSuivante = p;
                    }
                }
                else{
                    if (pSuivante.getAnimalParSexe("Mâle").size() < p.getAnimalParSexe("Mâle").size()){
                        pSuivante = p;
                    }
                }
            }

            return pSuivante;
        }
        else if (prairieCourante.getI() > 0 && prairieCourante.getI() < matricePrairie.length-1 && prairieCourante.getJ() <= 0 ){
            Prairie iPlus1 = matricePrairie[prairieCourante.getI()+1][prairieCourante.getJ()];
            Prairie iMoin1 = matricePrairie[prairieCourante.getI()-1][prairieCourante.getJ()];
            Prairie jPlus1 = matricePrairie[prairieCourante.getI()][prairieCourante.getJ()+1];

            List<Prairie> listePrairieSuivante = new ArrayList<>();
            listePrairieSuivante.add(iPlus1);
            listePrairieSuivante.add(iMoin1);
            listePrairieSuivante.add(jPlus1);

            Prairie pSuivante = listePrairieSuivante.get(r.nextInt(listePrairieSuivante.size())) ; 
            for (Prairie p : listePrairieSuivante){
                if (animal.getSexe() == "Mâle"){
                    if (pSuivante.getAnimalParSexe("Femelle").size() < p.getAnimalParSexe("Femelle").size()){
                        pSuivante = p;
                    }
                }
                else{
                    if (pSuivante.getAnimalParSexe("Mâle").size() < p.getAnimalParSexe("Mâle").size()){
                        pSuivante = p;
                    }
                }
            }

            return pSuivante;
        }
        else if (prairieCourante.getI() > 0 && prairieCourante.getI() < matricePrairie.length-1 && prairieCourante.getJ() >= matricePrairie[0].length) {
            Prairie iPlus1 = matricePrairie[prairieCourante.getI()+1][prairieCourante.getJ()];
            Prairie iMoin1 = matricePrairie[prairieCourante.getI()-1][prairieCourante.getJ()];
            Prairie jMoin1 = matricePrairie[prairieCourante.getI()][prairieCourante.getJ()-1];

            List<Prairie> listePrairieSuivante = new ArrayList<>();
            listePrairieSuivante.add(iPlus1);
            listePrairieSuivante.add(iMoin1);
            listePrairieSuivante.add(jMoin1);

            Prairie pSuivante = listePrairieSuivante.get(r.nextInt(listePrairieSuivante.size())) ; 
            for (Prairie p : listePrairieSuivante){
                if (animal.getSexe() == "Mâle"){
                    if (pSuivante.getAnimalParSexe("Femelle").size() < p.getAnimalParSexe("Femelle").size()){
                        pSuivante = p;
                    }
                }
                else{
                    if (pSuivante.getAnimalParSexe("Mâle").size() < p.getAnimalParSexe("Mâle").size()){
                        pSuivante = p;
                    }
                }
            }

            return pSuivante;
        }
        else if (prairieCourante.getI() <= 0 && prairieCourante.getJ() <= 0){
            Prairie iPlus1 = matricePrairie[prairieCourante.getI()+1][prairieCourante.getJ()];
            Prairie jPlus1 = matricePrairie[prairieCourante.getI()][prairieCourante.getJ()+1];

            List<Prairie> listePrairieSuivante = new ArrayList<>();
            listePrairieSuivante.add(iPlus1);
            listePrairieSuivante.add(jPlus1);

            Prairie pSuivante = listePrairieSuivante.get(r.nextInt(listePrairieSuivante.size())) ; 
            for (Prairie p : listePrairieSuivante){
                if (animal.getSexe() == "Mâle"){
                    if (pSuivante.getAnimalParSexe("Femelle").size() < p.getAnimalParSexe("Femelle").size()){
                        pSuivante = p;
                    }
                }
                else{
                    if (pSuivante.getAnimalParSexe("Mâle").size() < p.getAnimalParSexe("Mâle").size()){
                        pSuivante = p;
                    }
                }
            }

            return pSuivante;
        }
        else if (prairieCourante.getI() <= 0 && prairieCourante.getJ() >= matricePrairie[0].length-1){
            Prairie iPlus1 = matricePrairie[prairieCourante.getI()+1][prairieCourante.getJ()];
            Prairie jMoin1 = matricePrairie[prairieCourante.getI()][prairieCourante.getJ()-1];

            List<Prairie> listePrairieSuivante = new ArrayList<>();
            listePrairieSuivante.add(iPlus1);
            listePrairieSuivante.add(jMoin1);

            Prairie pSuivante = listePrairieSuivante.get(r.nextInt(listePrairieSuivante.size())) ; 
            for (Prairie p : listePrairieSuivante){
                if (animal.getSexe() == "Mâle"){
                    if (pSuivante.getAnimalParSexe("Femelle").size() < p.getAnimalParSexe("Femelle").size()){
                        pSuivante = p;
                    }
                }
                else{
                    if (pSuivante.getAnimalParSexe("Mâle").size() < p.getAnimalParSexe("Mâle").size()){
                        pSuivante = p;
                    }
                }
            }

            return pSuivante;
        }
        else if (prairieCourante.getI() >= matricePrairie.length-1 && prairieCourante.getJ() <= 0){
            Prairie iMoin1 = matricePrairie[prairieCourante.getI()-1][prairieCourante.getJ()];
            Prairie jPlus1 = matricePrairie[prairieCourante.getI()][prairieCourante.getJ()+1];

            List<Prairie> listePrairieSuivante = new ArrayList<>();
            listePrairieSuivante.add(iMoin1);
            listePrairieSuivante.add(jPlus1);

            Prairie pSuivante = listePrairieSuivante.get(r.nextInt(listePrairieSuivante.size())) ; 
            for (Prairie p : listePrairieSuivante){
                if (animal.getSexe() == "Mâle"){
                    if (pSuivante.getAnimalParSexe("Femelle").size() < p.getAnimalParSexe("Femelle").size()){
                        pSuivante = p;
                    }
                }
                else{
                    if (pSuivante.getAnimalParSexe("Mâle").size() < p.getAnimalParSexe("Mâle").size()){
                        pSuivante = p;
                    }
                }
            }

            return pSuivante;
        }
        else{
            Prairie iMoin1 = matricePrairie[prairieCourante.getI()-1][prairieCourante.getJ()];
            Prairie jMoin1 = matricePrairie[prairieCourante.getI()][prairieCourante.getJ()-1];



            List<Prairie> listePrairieSuivante = new ArrayList<>();
            listePrairieSuivante.add(iMoin1);
            listePrairieSuivante.add(jMoin1);

            Prairie pSuivante = listePrairieSuivante.get(r.nextInt(listePrairieSuivante.size())) ; 
            for (Prairie p : listePrairieSuivante){
                if (animal.getSexe() == "Mâle"){
                    if (pSuivante.getAnimalParSexe("Femelle").size() < p.getAnimalParSexe("Femelle").size()){
                        pSuivante = p;
                    }
                }
                else{
                    if (pSuivante.getAnimalParSexe("Mâle").size() < p.getAnimalParSexe("Mâle").size()){
                        pSuivante = p;
                    }
                }
            }

            return pSuivante;
        }
        


    }


    public Prairie trouverNourriture(Animal animal){
        
        Prairie prairieCourante = animal.getPrairieCourante();
        Monde monde = prairieCourante.getMonde();
        Prairie[][] matricePrairie = monde.getPrairies();
        Random r = new Random();

        
        if (prairieCourante.getI() > 0 && prairieCourante.getI() < matricePrairie.length-1 && prairieCourante.getJ() > 0 && prairieCourante.getJ() < matricePrairie[0].length-1){
            Prairie iPlus1 = matricePrairie[prairieCourante.getI()+1][prairieCourante.getJ()];
            Prairie iMoin1 = matricePrairie[prairieCourante.getI()-1][prairieCourante.getJ()];
            Prairie jPlus1 = matricePrairie[prairieCourante.getI()][prairieCourante.getJ()+1];
            Prairie jMoin1 = matricePrairie[prairieCourante.getI()][prairieCourante.getJ()-1];

            List<Prairie> listePrairieSuivante = new ArrayList<>();
            listePrairieSuivante.add(iPlus1);
            listePrairieSuivante.add(iMoin1);
            listePrairieSuivante.add(jPlus1);
            listePrairieSuivante.add(jMoin1);

            Prairie pSuivante = listePrairieSuivante.get(r.nextInt(listePrairieSuivante.size())) ; 
            for (Prairie p : listePrairieSuivante){
                if (pSuivante.getNbNourriture() < p.getNbNourriture()){
                    pSuivante = p;
                }
            }

            return pSuivante;
        }
        
        else if (prairieCourante.getI() <= 0 && prairieCourante.getJ() > 0 && prairieCourante.getJ() < matricePrairie[0].length-1){
            Prairie iPlus1 = matricePrairie[prairieCourante.getI()+1][prairieCourante.getJ()];
            Prairie jPlus1 = matricePrairie[prairieCourante.getI()][prairieCourante.getJ()+1];
            Prairie jMoin1 = matricePrairie[prairieCourante.getI()][prairieCourante.getJ()-1];
            
            List<Prairie> listePrairieSuivante = new ArrayList<>();
            listePrairieSuivante.add(iPlus1);
            listePrairieSuivante.add(jPlus1);
            listePrairieSuivante.add(jMoin1);

            Prairie pSuivante = listePrairieSuivante.get(r.nextInt(listePrairieSuivante.size())) ; 
            for (Prairie p : listePrairieSuivante){
                if (pSuivante.getNbNourriture() < p.getNbNourriture()){
                    pSuivante = p;
                }
            }

            return pSuivante;
        }
        else if(prairieCourante.getI() >= matricePrairie.length-1 && prairieCourante.getJ() > 0 && prairieCourante.getJ() < matricePrairie[0].length-1){
            Prairie iMoin1 = matricePrairie[prairieCourante.getI()-1][prairieCourante.getJ()];
            Prairie jPlus1 = matricePrairie[prairieCourante.getI()][prairieCourante.getJ()+1];
            Prairie jMoin1 = matricePrairie[prairieCourante.getI()][prairieCourante.getJ()-1];

            List<Prairie> listePrairieSuivante = new ArrayList<>();
            listePrairieSuivante.add(iMoin1);
            listePrairieSuivante.add(jPlus1);
            listePrairieSuivante.add(jMoin1);

            Prairie pSuivante = listePrairieSuivante.get(r.nextInt(listePrairieSuivante.size())) ; 
            for (Prairie p : listePrairieSuivante){
                if (pSuivante.getNbNourriture() < p.getNbNourriture()){
                    pSuivante = p;
                }
            }

            return pSuivante;
        }
        else if (prairieCourante.getI() > 0 && prairieCourante.getI() < matricePrairie.length-1 && prairieCourante.getJ() <= 0 ){
            Prairie iPlus1 = matricePrairie[prairieCourante.getI()+1][prairieCourante.getJ()];
            Prairie iMoin1 = matricePrairie[prairieCourante.getI()-1][prairieCourante.getJ()];
            Prairie jPlus1 = matricePrairie[prairieCourante.getI()][prairieCourante.getJ()+1];

            List<Prairie> listePrairieSuivante = new ArrayList<>();
            listePrairieSuivante.add(iPlus1);
            listePrairieSuivante.add(iMoin1);
            listePrairieSuivante.add(jPlus1);

            Prairie pSuivante = listePrairieSuivante.get(r.nextInt(listePrairieSuivante.size())) ; 
            for (Prairie p : listePrairieSuivante){
                if (pSuivante.getNbNourriture() < p.getNbNourriture()){
                    pSuivante = p;
                }
            }

            return pSuivante;
        }
        else if (prairieCourante.getI() > 0 && prairieCourante.getI() < matricePrairie.length-1 && prairieCourante.getJ() >= matricePrairie[0].length) {
            Prairie iPlus1 = matricePrairie[prairieCourante.getI()+1][prairieCourante.getJ()];
            Prairie iMoin1 = matricePrairie[prairieCourante.getI()-1][prairieCourante.getJ()];
            Prairie jMoin1 = matricePrairie[prairieCourante.getI()][prairieCourante.getJ()-1];

            List<Prairie> listePrairieSuivante = new ArrayList<>();
            listePrairieSuivante.add(iPlus1);
            listePrairieSuivante.add(iMoin1);
            listePrairieSuivante.add(jMoin1);

            Prairie pSuivante = listePrairieSuivante.get(r.nextInt(listePrairieSuivante.size())) ; 
            for (Prairie p : listePrairieSuivante){
                if (pSuivante.getNbNourriture() < p.getNbNourriture()){
                    pSuivante = p;
                }
            }

            return pSuivante;
        }
        else if (prairieCourante.getI() <= 0 && prairieCourante.getJ() <= 0){
            Prairie iPlus1 = matricePrairie[prairieCourante.getI()+1][prairieCourante.getJ()];
            Prairie jPlus1 = matricePrairie[prairieCourante.getI()][prairieCourante.getJ()+1];

            List<Prairie> listePrairieSuivante = new ArrayList<>();
            listePrairieSuivante.add(iPlus1);
            listePrairieSuivante.add(jPlus1);

            Prairie pSuivante = listePrairieSuivante.get(r.nextInt(listePrairieSuivante.size())) ; 
            for (Prairie p : listePrairieSuivante){
                if (pSuivante.getNbNourriture() < p.getNbNourriture()){
                    pSuivante = p;
                }
            }

            return pSuivante;
        }
        else if (prairieCourante.getI() <= 0 && prairieCourante.getJ() >= matricePrairie[0].length-1){
            Prairie iPlus1 = matricePrairie[prairieCourante.getI()+1][prairieCourante.getJ()];
            Prairie jMoin1 = matricePrairie[prairieCourante.getI()][prairieCourante.getJ()-1];

            List<Prairie> listePrairieSuivante = new ArrayList<>();
            listePrairieSuivante.add(iPlus1);
            listePrairieSuivante.add(jMoin1);

            Prairie pSuivante = listePrairieSuivante.get(r.nextInt(listePrairieSuivante.size())) ; 
            for (Prairie p : listePrairieSuivante){
                if (pSuivante.getNbNourriture() < p.getNbNourriture()){
                    pSuivante = p;
                }
            }

            return pSuivante;
        }
        else if (prairieCourante.getI() >= matricePrairie.length-1 && prairieCourante.getJ() <= 0){
            Prairie iMoin1 = matricePrairie[prairieCourante.getI()-1][prairieCourante.getJ()];
            Prairie jPlus1 = matricePrairie[prairieCourante.getI()][prairieCourante.getJ()+1];

            List<Prairie> listePrairieSuivante = new ArrayList<>();
            listePrairieSuivante.add(iMoin1);
            listePrairieSuivante.add(jPlus1);

            Prairie pSuivante = listePrairieSuivante.get(r.nextInt(listePrairieSuivante.size())) ; 
            for (Prairie p : listePrairieSuivante){
                if (pSuivante.getNbNourriture() < p.getNbNourriture()){
                    pSuivante = p;
                }
            }

            return pSuivante;
        }
        else{
            Prairie iMoin1 = matricePrairie[prairieCourante.getI()-1][prairieCourante.getJ()];
            Prairie jMoin1 = matricePrairie[prairieCourante.getI()][prairieCourante.getJ()-1];



            List<Prairie> listePrairieSuivante = new ArrayList<>();
            listePrairieSuivante.add(iMoin1);
            listePrairieSuivante.add(jMoin1);

            Prairie pSuivante = listePrairieSuivante.get(r.nextInt(listePrairieSuivante.size())) ; 
            for (Prairie p : listePrairieSuivante){
                if (pSuivante.getNbNourriture() < p.getNbNourriture()){
                    pSuivante = p;
                }
            }

            return pSuivante;
        }
        
    }

}

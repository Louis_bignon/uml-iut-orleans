public class AdulteAffame implements IEtatAnimal {
 

    public void deplacement(AutomateAnimal automateA, ISeDeplacer methodeDeplacer){
        Animal animal = automateA.getAnimal();
        if (animal.getSolde() == 0){
            IEtatAnimal animalMort = new AnimalMort();
            automateA.changementEtat(animalMort);
        }
        
        animal.seDeplacer(methodeDeplacer);

    }


    public void nourir(AutomateAnimal automateB, ISeNourir methodeSeNourir){
        Animal animal = automateB.getAnimal();
        animal.seNourir(methodeSeNourir);
        if (animal.getSolde() >= 20){
            IEtatAnimal adulteRassasie = new AdulteRassasie();
            automateB.changementEtat(adulteRassasie);
        }
    }

    public void reproduire(AutomateAnimal automateC){}



    @Override
    public String toString(){
        return "AdulteAffame";
    }
}

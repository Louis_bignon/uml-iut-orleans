import java.util.List;
import java.util.ArrayList;
import java.util.Random;

public class Monde{

    private static Monde instance = null;
    private int lignes;
    private int colonnes;
    private Prairie[][] prairies;
    private List<Animal> listeAnimal;
    


    private Monde(int lignes, int colonnes){

        this.lignes = lignes;
        this.colonnes = colonnes;
        this.listeAnimal = new ArrayList<>();
        this.prairies = new Prairie[this.lignes][this.colonnes];

        for ( int i=0 ; i<this.prairies.length;i++){
            for (int j=0 ; j < this.prairies[i].length; j++){
                this.prairies[i][j] = new Prairie(i,j,this);
            }
        }
        
    }

    public static Monde getInstance(int lignes , int colonnes){
        if(instance == null){
            instance = new Monde(lignes,colonnes);
        }
        return instance;
    }

    public void faitreNaitreAnimal(Animal animal){
        Random r = new Random();
        int randomI  = r.nextInt(this.lignes);
        int randomJ = r.nextInt(this.colonnes);
        this.listeAnimal.add(animal);
        this.prairies[randomI][randomJ].addAnimal(animal);

    }



    public void Nouvellejourne(){
        List<Animal> animalToRemove = new ArrayList<>();
        for ( int i=0 ; i<this.prairies.length;i++){
            for (int j=0 ; j < this.prairies[i].length; j++){
                this.prairies[i][j].pousser();
                for (Animal a : this.prairies[i][j].getAnimal()){
                    if (a.getMort() == true){
                        animalToRemove.add(a);
                        this.listeAnimal.remove(a);
                    }
                    else if (! this.listeAnimal.contains(a)){
                        this.listeAnimal.add(a);
                    }
                }
            }
        }  
        
        for(Animal a : animalToRemove){
            a.getPrairieCourante().removeAnimal(a);
        }

        for(Animal a : this.listeAnimal){

            a.passerJourne();
            a.augmenterAge(); 


        }
    }

    public Prairie[][] getPrairies(){
        return this.prairies;
    }

    public int getLignes(){
        return this.lignes;
    }
    public int getColonnes(){
        return this.colonnes;
    }
    public List<Animal> getAnimaux(){
        return this.listeAnimal;
    }


    @Override
    public String toString(){

        String res = "";
        for ( int i=0 ; i<this.prairies.length ;i++){
            res += "|\n";
            for (int j=0 ; j < this.prairies[0].length; j++){
                res += this.prairies[i][j].toString();
                
            }
            res += "\n";

        }

        return res;
    }


}
import java.util.List;
import java.util.Random;


public class Carnivore implements ISeNourir{
    
    public void seNourir(Animal animal){
        Prairie prairieCourante = animal.getPrairieCourante();
        List<Animal> listeHerbivore = prairieCourante.getListeParRace("Lapin");

        if(listeHerbivore.size() != 0){

            for (Animal herbivore : listeHerbivore){
                prairieCourante.removeAnimal(herbivore);
            }
            
            animal.augmenterEnergie(3*listeHerbivore.size());
            

        }
    }


}
